Role Name
=========

A role that installs the R environment, <https://cran.r-project.org> 

Role Variables
--------------

The most important variables are listed below:

``` yaml
# Set to True if you want install from the CRAN deb repository
r_install_cran_repo: False
# Use 'cran35' for R 3.6
r_cran_repo_version: 'cran40'
r_cran_repo_url: 'https://cloud.r-project.org/bin/linux/ubuntu {{ ansible_distribution_release }}-{{ r_cran_repo_version }}/'
# Use 'marutter/c2d4u3.5' for R 3.6
r_cran_ubuntu_packages_ppa: 'ppa:marutter/c2d4u'
#r_cran_mirror_site: http://cran.rstudio.com
r_cran_set_default_mirror: True
r_cran_mirror_site: https://cran.mirror.garr.it/mirrors/CRAN/
r_base_specific_version: False
r_base_pkg_version: 3.4.4
r_packages_main_state: present
r_packages_state: '{{ r_packages_main_state }}'
r_sitelib_path: '/usr/local/lib/R/site-library'
r_plugins_from_deb: True
r_packages_cleanup: False
#
r_packages_updater: False
r_package_updater_via_subversion: True
r_package_updater_subversion_repo: http://svn.research-infrastructures.eu/public/d4science/gcube/trunk/data-analysis/RConfiguration/RPackagesManagement/
r_packages_svn_base_dir: /srv/r_updater
r_packages_svn_files_prefix: ''
r_packages_updater_also_upgrades_installed_packages: False
# They need to be flat text files
# 1 package per line
#r_debian_packages_list_url
# package[:cran mirror]
# The CRAN mirror URL is optional
#r_cran_packages_list_url
# The CRAN mirror URL is optional
#r_cran_packages_list_url
# user/package_name
#r_github_packages_list_url

r_source_plugins_dest_dir: /var/cache/R

r_distribution_required_packages: []
#   - gdal-bin
#   - dans-gdal-scripts
#   - libgdal1-dev
#   - libgeos-dev
#   - libspatialite-dev
#   - proj
#   - proj-bin
#   - proj-data
#   - libproj-dev

r_base_packages_list:
  - r-base

# Some packages must be put on hold
r_base_packages_hold_list: []
  
r_plugins_packages_list:
  - jags
  - r-cran-rjags
  - r-cran-abind
  - r-cran-boot
  - r-cran-class
  - r-cran-cluster
  - r-cran-coda
  - r-cran-codetools
  - r-cran-foreign
  - r-cran-lattice
  - r-cran-maptools
  - r-cran-mass
  - r-cran-matrix
  - r-cran-mgcv
  - r-cran-nlme
  - r-cran-nnet
  - r-cran-rpart
  - r-cran-sp
  - r-cran-spatial
  - r-cran-survival

r_apt_additional_repos:
  - 'ppa:opencpu/jq'

# RHEL/CentOS:
# http://springdale.princeton.edu/data/springdale/7/x86_64/os/Computational/
#
r_sitelib_el_path: '/usr/local/lib/R/library'
r_el_dev_pkgs: True
r_base_packages_el:
  - R
  - R-core
  - libRmath

r_base_packages_dev_el:
  - R-core-devel
  - libRmath-devel

r_base_packages_java_el:
  - R-java

r_base_packages_java_dev_el:
  - R-java-devel

r_plugins_list_to_install: []
#   - { name: 'R2WinBUGS', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'R2jags', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'bayesmix', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'coda', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'rjags', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'runjags', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'base', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'compiler', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'datasets', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'grDevices', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'graphics', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'grid', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'methods', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'parallel', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'reshape', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'splines', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'stats', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'stats4', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'tcltk', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'tools', repo: '{{ r_cran_mirror_site }}' }
#   - { name: 'utils', repo: '{{ r_cran_mirror_site }}' }

r_plugins_from_sources: []
#   - { name: 'plyr', version: '1.7.1', source: 'plyr_1.7.1.tar.gz', url: 'http://cran.r-project.org/src/contrib/Archive/plyr/plyr_1.7.1.tar.gz' }

r_plugins_from_github: []
#   - { plugin_name: 'RFigisGeo', github_user: 'openfigis' }
#   - { plugin_name: 'rsdmx', github_user: 'opensdmx' }

# 
r_plugins_list_to_remove: []
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
